//
//  pinViewController.swift
//  inputValidations
//
//  Created by Taslima Roya on 6/7/20.
//  Copyright © 2020 Taslima Roya. All rights reserved.
//

import UIKit

class pinViewController: UIViewController {
    @IBOutlet weak var passwordValidation: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func passwordButton(_ sender: UIButton) {
        
        if (passwordValidation.text?.isPasswordValid)!{
            print("password is valid")
        } else{
            print("password is not valid")
        }
    }
   

}

extension String{
    
    var isPasswordValid:Bool{
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Z a-z\\d$@$#!%*?&]{6}")
        return passwordTest.evaluate(with: self)
    }
    
    
}
