//
//  mobileViewController.swift
//  inputValidations
//
//  Created by Taslima Roya on 6/7/20.
//  Copyright © 2020 Taslima Roya. All rights reserved.
//

import UIKit

class mobileViewController: UIViewController {

    @IBOutlet weak var phoneValidation: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    @IBAction func phoneNext(_ sender: UIButton) {
        
        if (phoneValidation.text?.isPhoneNumber)!{
                          print("phone number is valid")
                      } else{
                          print("phone number is not valid")
                      }
               
           
    }
    
 

}

extension String{

var isPhoneNumber:Bool {
    
    do{
        
        let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
        
        let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
        
        if let res = matches.first{
            
            return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count && self.count == 10
        } else {
            return false
        }
    } catch {
        return false
    }
}
}
